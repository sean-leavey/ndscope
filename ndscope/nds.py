from __future__ import division
import time
import numpy as np
from contextlib import closing
try:
    from qtpy import QtCore
    from qtpy.QtCore import Signal
except ImportError:
    from PyQt5 import QtCore
    from PyQt5.QtCore import pyqtSignal as Signal
import logging

import nds2
from gpstime import gpsnow

from . import const

##########

# ctypes: 'online', 's-trend', 'm-trend'
# mods: 'raw', 'min', 'max', 'mean'

TREND_CTYPES = [
    nds2.channel.CHANNEL_TYPE_STREND,
    nds2.channel.CHANNEL_TYPE_MTREND,
]

##########


def _parse_channel(channel):
    ct = channel.split(',')
    name = ct[0]
    if len(ct) > 1:
        if ct[1] == 's-trend':
            ctype = nds2.channel.CHANNEL_TYPE_STREND
            sample_rate = 1
        elif ct[1] == 'm-trend':
            ctype = nds2.channel.CHANNEL_TYPE_MTREND
            sample_rate = 1/60
    elif 'MON' in name:
        ctype = nds2.channel.CHANNEL_TYPE_RAW
        sample_rate = 16
    else:
        ctype = nds2.channel.CHANNEL_TYPE_RAW
        sample_rate = 2**14
    return name, ctype, sample_rate


class FakeChannel:
    def __init__(self, name, ctype, sample_rate):
        self.name = name
        self.channel_type = ctype
        self.sample_rate = sample_rate

    def DataTypeSize(self):
        return 8


class FakeBuffer:
    def __init__(self, name, ctype, sample_rate, seconds, nanoseconds, data):
        self.channel = FakeChannel(name, ctype, sample_rate)
        assert isinstance(seconds, int)
        assert isinstance(nanoseconds, int)
        self.gps_seconds = seconds
        self.gps_nanoseconds = nanoseconds
        self.data = data


class FakeChannelSource:
    def __init__(self, channel):
        self.channel = channel
        self.amp = np.random.normal(10, 5, 1)
        self.freq = 2*np.pi * np.random.normal(1, 1/2, 1)
        self.phase = 2*np.pi * np.random.random()
        self.offset = np.random.normal(0, 20, 1)

    def sampler(self, t):
        # signal
        data = self.amp*np.sin(t * self.freq + self.phase)
        # add offset
        data += self.offset
        # add noise
        # data += np.random.normal(0, 1, len(t))
        data += np.random.exponential(3, len(t))
        # add glitches
        #data += 20 * np.random.power(0.1, len(t)) * np.random.choice([-1,1])
        # add a gap
        if np.random.randint(0, 100) == 0:
            data *= np.nan
        return data

    def gen_buf(self, ctype, sample_rate, start, stride):
        seconds = int(start)
        nanoseconds = int((start % 1) * 1e9)
        nsamples = int(sample_rate*stride)
        t = np.arange(nsamples)/sample_rate + seconds + nanoseconds*1e-9
        data = self.sampler(t)
        return FakeBuffer(
            self.channel,
            ctype,
            sample_rate,
            seconds,
            nanoseconds,
            data,
        )


class FakeSource:
    def __init__(self):
        self.sources = {}

    def gen_bufs(self, channels, start, stride):
        bufs = []
        for chan in channels:
            name, ctype, sample_rate = _parse_channel(chan)
            if name not in self.sources:
                self.sources[name] = FakeChannelSource(name)
            bufs.append(self.sources[name].gen_buf(
                ctype,
                sample_rate,
                start,
                stride,
            ))
        return bufs


FAKE_SOURCE = FakeSource()


class FakeConnection:
    def set_parameter(self, *args):
        return

    def get_protocol(self):
        return 1

    def fetch(self, start, end, channels):
        global FAKE_SOURCE
        stride = end - start
        return FAKE_SOURCE.gen_bufs(channels, start, stride)

    def iterate(self, stride, channels):
        global FAKE_SOURCE
        if stride == -1:
            stride = 1./16
        start = np.ceil(gpsnow())
        while True:
            start += stride
            sleep = start - gpsnow()
            if sleep > 0:
                time.sleep(sleep)
            yield FAKE_SOURCE.gen_bufs(channels, start, stride)

    def close(self):
        return

##########


def get_connection():
    logging.debug("NDS connect: {}:{}".format(const.HOST, const.PORT))
    if const.HOST == 'fake':
        conn = FakeConnection()
    else:
        conn = nds2.connection(const.HOST, const.PORT)
    conn.set_parameter('GAP_HANDLER', 'STATIC_HANDLER_NAN')
    # conn.set_parameter('ITERATE_USE_GAP_HANDLERS', 'false')
    return conn


def iterate(channels, start_end=None):
    args = []
    if start_end:
        args += list(start_end)
    # FIXME: nds2 0.16 has the following:
    # args += [nds2.connection.FAST_STRIDE]
    # args += [channels]
    with closing(get_connection()) as conn:
        if conn.get_protocol() == 1:
            stride = -1
        else:
            stride = 1
        args += [stride]
        args += [channels]
        logging.log(5, "NDS iterate{}".format(tuple(args)))
        for bufs in conn.iterate(*args):
            yield bufs


def fetch(channels, start_end):
    args = list(start_end) + [channels]
    with closing(get_connection()) as conn:
        logging.log(5, "NDS fetch{}".format(tuple(args)))
        return conn.fetch(*args)


def parse_channel(channel):
    ctype = nds2.channel.channel_type_to_string(channel.channel_type)
    if ctype in ['s-trend', 'm-trend']:
        # HACK: FIXME: work around a bug in nds2-client around 0.16.3:
        # https://git.ligo.org/nds/nds2-client/issues/85. this should
        # not be necessary (to split on ',') and should be removed
        # once the client is fixed.
        namemod = channel.name.split(',')[0]
        name, mod = namemod.split('.')
    else:
        name = channel.name
        mod = 'raw'
    return name, mod, ctype

##################################################


class NDSThread(QtCore.QThread):
    new_data = Signal('PyQt_PyObject')
    done = Signal('PyQt_PyObject')

    _type_map = {'raw': None, 'sec': 's', 'min': 'm'}

    def __init__(self, tid, trend, cmd, channels, start_end=None):
        super(NDSThread, self).__init__()
        self.tid = tid
        self.trend = trend
        self.cmd = cmd
        if self.cmd == 'online':
            self.method = 'iterate'
        else:
            self.method = 'fetch'
        if self.method == 'fetch':
            assert start_end is not None
        self.channels = []
        for chan in channels:
            if trend == 'raw':
                self.channels.append(chan)
            else:
                for m in ['mean', 'min', 'max']:
                    t = self._type_map[self.trend]
                    self.channels.append('{}.{},{}-trend'.format(chan, m, t))
        if start_end:
            start = int(start_end[0])
            end = int(np.ceil(start_end[1]))
            if trend == 'min':
                start -= (start % 60)
                end += 60 - (end % 60)
            assert start < end, "invalid times: {} >= {}".format(start, end)
            self.start_end = (start, end)
        else:
            self.start_end = None
        self._run_lock = QtCore.QMutex()
        self._running = True

    def emit_data(self, bufs):
        logging.log(5, "NDS {} {} buf: {}.{:09}".format(self.trend, self.cmd, bufs[0].gps_seconds, bufs[0].gps_nanoseconds))
        self.new_data.emit((self.trend, self.cmd, bufs))

    def emit_done(self, error=None):
        self.done.emit((self.tid, error))

    @property
    def running(self):
        try:
            self._run_lock.lock()
        # FIXME: python3
        #else:
            return self._running
        finally:
            self._run_lock.unlock()

    def run(self):
        error = None

        if self.method == 'fetch':
            try:
                bufs = fetch(self.channels, self.start_end)
                self.emit_data(bufs)
            except RuntimeError as e:
                error = str(e).split('\n')[0]
            # HACK: FIXME: catch TypeError here because of a bug in
            # the client that started around 0.16.3, that is actually
            # exposing a bug in the NDS1 server:
            # https://git.ligo.org/cds/ndscope/issues/109.  Quick
            # successive fetches cause the server to start returning
            # garbage, that shows up as a TypeError in the client
            except TypeError as e:
                error = str(e).split('\n')[0]

        elif self.method == 'iterate':
            try:
                for bufs in iterate(self.channels, self.start_end):
                    if not self.running:
                        break
                    self.emit_data(bufs)
                    if not self.running:
                        break
            except RuntimeError as e:
                error = str(e).split('\n')[0]

        self.emit_done(error)

    def stop(self):
        self._run_lock.lock()
        self._running = False
        self._run_lock.unlock()
