from setuptools import setup


setup(
    setup_requires=[
        'setuptools_scm',
    ],
    use_scm_version={
        'write_to': 'ndscope/version.py',
    },

    name='ndscope',
    description="Next-generation NDS time series plotting",
    author='Jameson Graef Rollins',
    author_email='jameson.rollins@ligo.org',
    url='https://git.ligo.org/cds/ndscope',
    license='GNU GPL v3+',

    # install_requires=[
    #     'PyQt5',
    #     'pyqtgraph',
    #     'nds2-client',
    #     'numpy',
    #     'gpstime',
    #     'yaml',
    # ],

    packages=[
        'ndscope',
        'ndscope.test',
    ],

    package_data={
        'ndscope': ['*.ui'],
    },

    entry_points={
        'console_scripts': [
            'ndscope = ndscope.__main__:main',
        ],
    },
)
